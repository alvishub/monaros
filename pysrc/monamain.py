#!/usr/bin/env python
import rospy
import math
import serial
#import numpy as np
#import scipy as sp
import sys
import time

from geometry_msgs.msg import Twist as geoTwist
from std_msgs.msg import Float32MultiArray as stdFloat32

ser = serial.Serial(port='/dev/serial0', baudrate =38400, timeout=3.0)

command = geoTwist()

def command_callback(cmd_data):
	global command
	command = cmd_data

def read_from_ardumona():
	in_data = ser.readline()
	str_data = str(in_data)
	if(ser.in_waiting <= 0):
		ser.flushInput()
	return str_data

def main(args):
	rospy.init_node('monamainpy', anonymous=True)
	rate = rospy.Rate(10) # 10-hz rate

	subRobotInfo = rospy.Subscriber("mona0/cmd_vel", geoTwist, command_callback)
	pubCommand = rospy.Publisher("mona0/IR/data_raw", stdFloat32, queue_size=1)	
	
	rospy.loginfo("READY")
	while not rospy.is_shutdown():
		t_start = rospy.get_time()
		temp_word_ser = 'L%d$A%d$' % (int(command.linear.x), int(command.angular.z))
		write_ser = temp_word_ser.encode('utf-8')
		ser.write(write_ser)
		if(ser.out_waiting <= 0):
			ser.flushOutput()
		
		# read_serial_data = read_from_ardumona()
		# rospy.loginfo(read_serial_data)		
		t_now = rospy.get_time() - t_start
		rate.sleep()

if __name__ == '__main__':
	main(sys.argv)
